
# Build Path
$(call inherit-product, $(LOCAL_PATH)/build_path.mk)

# Default Features
$(call inherit-product, $(LOCAL_PATH)/default_features.mk)

# Camera HAL
ifeq ($(FEATURE_CAMERA), true)
PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.5-service-thead \
    libthead_camera_core \
    libcsi_camera_plugin \
    libthead_jpeg \
    libyaml
endif

PRODUCT_PACKAGES += audio.primary.$(TARGET_BOARD_PLATFORM)

# Release key included only for user build
ifeq ($(TARGET_BUILD_VARIANT),user)
PRODUCT_DEFAULT_DEV_CERTIFICATE ?= vendor/xuantie/proprietary/security/apk_key
endif


